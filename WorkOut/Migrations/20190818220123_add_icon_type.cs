﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class add_icon_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Avatar",
                table: "ActivityTypes",
                newName: "Icon");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Icon",
                table: "ActivityTypes",
                newName: "Avatar");
        }
    }
}
