﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class updatetwocolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Workouts",
                nullable: true);

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "Temp",
                table: "DistanceWorkouts",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Workouts");

            migrationBuilder.AlterColumn<string>(
                name: "Temp",
                table: "DistanceWorkouts",
                nullable: true,
                oldClrType: typeof(TimeSpan));
        }
    }
}
