﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class UpdateStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DistanceWorkout_Workouts_WorkoutId",
                table: "DistanceWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_NumberWorkout_Workouts_WorkoutId",
                table: "NumberWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_Sets_NumberWorkout_NumberWorkoutId",
                table: "Sets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NumberWorkout",
                table: "NumberWorkout");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DistanceWorkout",
                table: "DistanceWorkout");

            migrationBuilder.RenameTable(
                name: "NumberWorkout",
                newName: "NumberWorkouts");

            migrationBuilder.RenameTable(
                name: "DistanceWorkout",
                newName: "DistanceWorkouts");

            migrationBuilder.RenameIndex(
                name: "IX_NumberWorkout_WorkoutId",
                table: "NumberWorkouts",
                newName: "IX_NumberWorkouts_WorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_DistanceWorkout_WorkoutId",
                table: "DistanceWorkouts",
                newName: "IX_DistanceWorkouts_WorkoutId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NumberWorkouts",
                table: "NumberWorkouts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DistanceWorkouts",
                table: "DistanceWorkouts",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DistanceWorkouts_Workouts_WorkoutId",
                table: "DistanceWorkouts",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NumberWorkouts_Workouts_WorkoutId",
                table: "NumberWorkouts",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_NumberWorkouts_NumberWorkoutId",
                table: "Sets",
                column: "NumberWorkoutId",
                principalTable: "NumberWorkouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DistanceWorkouts_Workouts_WorkoutId",
                table: "DistanceWorkouts");

            migrationBuilder.DropForeignKey(
                name: "FK_NumberWorkouts_Workouts_WorkoutId",
                table: "NumberWorkouts");

            migrationBuilder.DropForeignKey(
                name: "FK_Sets_NumberWorkouts_NumberWorkoutId",
                table: "Sets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_NumberWorkouts",
                table: "NumberWorkouts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DistanceWorkouts",
                table: "DistanceWorkouts");

            migrationBuilder.RenameTable(
                name: "NumberWorkouts",
                newName: "NumberWorkout");

            migrationBuilder.RenameTable(
                name: "DistanceWorkouts",
                newName: "DistanceWorkout");

            migrationBuilder.RenameIndex(
                name: "IX_NumberWorkouts_WorkoutId",
                table: "NumberWorkout",
                newName: "IX_NumberWorkout_WorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_DistanceWorkouts_WorkoutId",
                table: "DistanceWorkout",
                newName: "IX_DistanceWorkout_WorkoutId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_NumberWorkout",
                table: "NumberWorkout",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DistanceWorkout",
                table: "DistanceWorkout",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DistanceWorkout_Workouts_WorkoutId",
                table: "DistanceWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NumberWorkout_Workouts_WorkoutId",
                table: "NumberWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_NumberWorkout_NumberWorkoutId",
                table: "Sets",
                column: "NumberWorkoutId",
                principalTable: "NumberWorkout",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
