﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class editstructure2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DistanceWorkout_Workouts_WorkoutId",
                table: "DistanceWorkout");

            migrationBuilder.DropForeignKey(
                name: "FK_NumberWorkout_Workouts_WorkoutId",
                table: "NumberWorkout");

            migrationBuilder.DropIndex(
                name: "IX_NumberWorkout_WorkoutId",
                table: "NumberWorkout");

            migrationBuilder.DropIndex(
                name: "IX_DistanceWorkout_WorkoutId",
                table: "DistanceWorkout");

            migrationBuilder.DropColumn(
                name: "WorkoutId",
                table: "NumberWorkout");

            migrationBuilder.DropColumn(
                name: "WorkoutId",
                table: "DistanceWorkout");

            migrationBuilder.AddColumn<int>(
                name: "DistanceWorkoutId",
                table: "Workouts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NumberWorkoutId",
                table: "Workouts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_DistanceWorkoutId",
                table: "Workouts",
                column: "DistanceWorkoutId",
                unique: true,
                filter: "[DistanceWorkoutId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_NumberWorkoutId",
                table: "Workouts",
                column: "NumberWorkoutId",
                unique: true,
                filter: "[NumberWorkoutId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_DistanceWorkout_DistanceWorkoutId",
                table: "Workouts",
                column: "DistanceWorkoutId",
                principalTable: "DistanceWorkout",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_NumberWorkout_NumberWorkoutId",
                table: "Workouts",
                column: "NumberWorkoutId",
                principalTable: "NumberWorkout",
                principalColumn: "NumberWorkoutId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_DistanceWorkout_DistanceWorkoutId",
                table: "Workouts");

            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_NumberWorkout_NumberWorkoutId",
                table: "Workouts");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_DistanceWorkoutId",
                table: "Workouts");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_NumberWorkoutId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "DistanceWorkoutId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "NumberWorkoutId",
                table: "Workouts");

            migrationBuilder.AddColumn<int>(
                name: "WorkoutId",
                table: "NumberWorkout",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkoutId",
                table: "DistanceWorkout",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NumberWorkout_WorkoutId",
                table: "NumberWorkout",
                column: "WorkoutId",
                unique: true,
                filter: "[WorkoutId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_DistanceWorkout_WorkoutId",
                table: "DistanceWorkout",
                column: "WorkoutId",
                unique: true,
                filter: "[WorkoutId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_DistanceWorkout_Workouts_WorkoutId",
                table: "DistanceWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NumberWorkout_Workouts_WorkoutId",
                table: "NumberWorkout",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
