﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class add_avatar_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Avatar",
                table: "ActivityTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "ActivityTypes");
        }
    }
}
