﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class editstructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sets_Workouts_WorkoutId",
                table: "Sets");

            migrationBuilder.RenameColumn(
                name: "DateTime",
                table: "Workouts",
                newName: "Pause");

            migrationBuilder.RenameColumn(
                name: "WorkoutId",
                table: "Sets",
                newName: "NumberWorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_Sets_WorkoutId",
                table: "Sets",
                newName: "IX_Sets_NumberWorkoutId");

            migrationBuilder.AddColumn<int>(
                name: "ActivityTypeId",
                table: "Workouts",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateTimeFinish",
                table: "Workouts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateTimeStart",
                table: "Workouts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "DistanceWorkout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Distance = table.Column<double>(nullable: false),
                    Temp = table.Column<string>(nullable: true),
                    WorkoutId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistanceWorkout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistanceWorkout_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NumberWorkout",
                columns: table => new
                {
                    NumberWorkoutId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WorkoutId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NumberWorkout", x => x.NumberWorkoutId);
                    table.ForeignKey(
                        name: "FK_NumberWorkout_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_ActivityTypeId",
                table: "Workouts",
                column: "ActivityTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DistanceWorkout_WorkoutId",
                table: "DistanceWorkout",
                column: "WorkoutId",
                unique: true,
                filter: "[WorkoutId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_NumberWorkout_WorkoutId",
                table: "NumberWorkout",
                column: "WorkoutId",
                unique: true,
                filter: "[WorkoutId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_NumberWorkout_NumberWorkoutId",
                table: "Sets",
                column: "NumberWorkoutId",
                principalTable: "NumberWorkout",
                principalColumn: "NumberWorkoutId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_ActivityTypes_ActivityTypeId",
                table: "Workouts",
                column: "ActivityTypeId",
                principalTable: "ActivityTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sets_NumberWorkout_NumberWorkoutId",
                table: "Sets");

            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_ActivityTypes_ActivityTypeId",
                table: "Workouts");

            migrationBuilder.DropTable(
                name: "DistanceWorkout");

            migrationBuilder.DropTable(
                name: "NumberWorkout");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_ActivityTypeId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "ActivityTypeId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "DateTimeFinish",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "DateTimeStart",
                table: "Workouts");

            migrationBuilder.RenameColumn(
                name: "Pause",
                table: "Workouts",
                newName: "DateTime");

            migrationBuilder.RenameColumn(
                name: "NumberWorkoutId",
                table: "Sets",
                newName: "WorkoutId");

            migrationBuilder.RenameIndex(
                name: "IX_Sets_NumberWorkoutId",
                table: "Sets",
                newName: "IX_Sets_WorkoutId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sets_Workouts_WorkoutId",
                table: "Sets",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
