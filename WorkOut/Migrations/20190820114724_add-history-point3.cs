﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WorkOut.Migrations
{
    public partial class addhistorypoint3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "HistoryPoints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "HistoryPoints");
        }
    }
}
