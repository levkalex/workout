﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Data.Identity
{
    public class WUserManager : UserManager<User>
    {
        IHistoryPointRepository historyPoint;
        public WUserManager(IUserStore<User> store, IHistoryPointRepository historyPoint, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<User> passwordHasher, IEnumerable<IUserValidator<User>> userValidators, IEnumerable<IPasswordValidator<User>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<User>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.historyPoint = historyPoint;
        }


        public async Task AddPointAsync(ClaimsPrincipal principal, int point, string type = "other", string info = "other")
        {
            var user = GetUserAsync(principal).Result;

            user.Point += point;

            await UpdateAsync(user);

            HistoryPoint history = new HistoryPoint { Count = point, UserId = user.Id, DateTime = DateTime.Now, Info = info, Type = type };
            historyPoint.Add(history);
        }
    }
}
