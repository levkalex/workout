﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.ViewModels
{
    public class CreateWorkoutViewModel
    {
        public Workout Workout { get; set; }
        public IEnumerable<ActivityTypeViewModel> ActivityTypes { get; set; }
    }
}
