﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.ViewModels
{
    public class ActivityTypeViewModel
    {
        public int Id { get; set; }
        public double Kcal { get; set; }
        public double Point { get; set; }
        public string CountingType { get; set; }
        public string NewName { get; set; }
        public byte[] Icon { get; set; }
    }
}
