﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Identity;
using WorkOut.Data.Models;

namespace WorkOut.Data.ViewModels
{
    public class ProfileViewModel
    {
        public User User { get; set; }
        public HistoryWeight CurrentWeight { get; set; }
        public IQueryable<ChartWeight> ChartWeights { get; set; }
    }

    public class ChartWeight
    {
        public string Date { get; set; }
        public double Weight { get; set; }
    }
}
