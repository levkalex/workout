﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.Interfaces
{
    public interface IWorkoutRepository
    {
        IQueryable<Workout> Workouts { get; }
        Workout GetById(int id);
        int AddNumber(Workout workout, Set[] sets);
        int AddDistance(Workout workout, DistanceWorkout distanceWorkout);
        int AddTime(Workout workout);
        int UpdateDistance(Workout workout, DistanceWorkout distanceWorkout);
        int UpdateNumber(Workout workout, Set[] sets);
        int UpdateTime(Workout workout);
        bool Delete(Workout workout);
        bool DeleteById(int Id);
    }
}
