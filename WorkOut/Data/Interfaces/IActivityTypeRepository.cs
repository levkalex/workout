﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.Interfaces
{
    public interface IActivityTypeRepository
    {
        IEnumerable<ActivityType> ActivityTypes { get; }
        ActivityType GetById(int Id);
        
        ActivityType GetByIdNoTracking(int Id);
        int Add(ActivityType type);
        bool Delete(ActivityType type);
        bool DeleteById(int Id);
        void Update(ActivityType type);

        bool IsNumberType(int typeId);
        bool IsDistanceType(int typeId);
    }
}
