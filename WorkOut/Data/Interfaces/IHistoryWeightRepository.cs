﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.Interfaces
{
    public interface IHistoryWeightRepository
    {
        IQueryable<HistoryWeight> HistoryWeights { get; }
        IQueryable<HistoryWeight> GetHistoryWeightsByUser(int userId);
        int Add(HistoryWeight historyWeight);  
    }
}
