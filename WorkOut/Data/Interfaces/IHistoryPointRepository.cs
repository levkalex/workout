﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.Interfaces
{
    public interface IHistoryPointRepository
    {
        IQueryable<HistoryPoint> HistoryPoints { get; }
        IQueryable<HistoryPoint> HistoryPointsByUser(int userId);
        int Add(HistoryPoint historyPoint);  
    }
}
