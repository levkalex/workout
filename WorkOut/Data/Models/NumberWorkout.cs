﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class NumberWorkout
    {
        public int Id { get; set; }

        public int? WorkoutId { get; set; }
        public Workout Workout { get; set; }

        public ICollection<Set> Sets { get; set; }
        public NumberWorkout()
        {
            Sets = new List<Set>();
        }
    }
}
