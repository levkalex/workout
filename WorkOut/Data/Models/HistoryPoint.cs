﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class HistoryPoint
    {
        public int Id { get; set; }

        public string Type { get; set; }
        public string Info { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }
        public int Count { get; set; }
    }
}
