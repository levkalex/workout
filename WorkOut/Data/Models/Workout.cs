﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public DateTime DateTimeStart { get; set; }
        public DateTime DateTimeFinish { get; set; }
        public int UserId { get; set; }
        public int Calouries { get; set; }
        public int Point { get; set; }
        public TimeSpan Pause { get; set; }
        public string Description { get; set; }

        public int? ActivityTypeId { get; set; }
        public ActivityType ActivityType { get; set; }


        public NumberWorkout NumberWorkout { get; set; }
        public DistanceWorkout DistanceWorkout { get; set; }
    }
}
