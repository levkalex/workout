﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class Set
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int Value { get; set; }
        public int Weight { get; set; }

        public int? NumberWorkoutId { get; set; }
        public NumberWorkout NumberWorkout { get; set; }
    }
}
