﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class HistoryWeight
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public DateTime DateTime { get; set; }
        public double Weight { get; set; }
    }
}
