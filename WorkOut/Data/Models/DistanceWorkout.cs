﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class DistanceWorkout
    {
        public int Id { get; set; }
        public double Distance { get; set; }
        public TimeSpan Temp { get; set; }

        public int? WorkoutId { get; set; }
        public Workout Workout { get; set; }
    }
}
