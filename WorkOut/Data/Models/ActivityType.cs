﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Models
{
    public class ActivityType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SubtypeName { get; set; }
        public string CountingType { get; set; }
        public double Kcal { get; set; }
        public double Point { get; set; }
        public byte[] Icon { get; set; }

        public ICollection<Workout> Workouts { get; set; }
        public ActivityType()
        {
            Workouts = new List<Workout>();
        }
    }
}
