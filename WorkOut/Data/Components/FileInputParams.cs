﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkOut.Data.Components
{
    public class FileInputParams
    {
        public string Accept { get; set; } = "";
        public int MaxHeight { get; set; } = 200;
        public string Image { get; set; } = "";
        public bool NewLine { get; set; } = true;
        public string Text { get; set; } = "Choose image";
    }
}
