﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Data.Database
{
    public class EFHistoryPointRepository : IHistoryPointRepository
    {
        private ApplicationDbContext context;

        public EFHistoryPointRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<HistoryPoint> HistoryPoints => context.HistoryPoints;

        public int Add(HistoryPoint historyPoint)
        {
            context.Add(historyPoint);
            context.SaveChanges();

            return historyPoint.Id;
        }

        public IQueryable<HistoryPoint> HistoryPointsByUser(int userId)
        {
            return (context.HistoryPoints.Where(x => x.UserId == userId));
        }
    }
}
