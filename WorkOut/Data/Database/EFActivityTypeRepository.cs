﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Data.Database
{
    public class EFActivityTypeRepository : IActivityTypeRepository
    {
        private ApplicationDbContext context;

        public EFActivityTypeRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<ActivityType> ActivityTypes => context.ActivityTypes;

        public int Add(ActivityType type)
        {
            context.Add(type);
            context.SaveChanges();

            return type.Id;
        }

        public bool IsNumberType(int typeId)
        {
            var a = ActivityTypes.Where(x => x.Id == typeId).FirstOrDefault();
            if (a.CountingType == "counter")
             return true;
            else return false;
        }

        public bool IsDistanceType(int typeId)
        {
            var a = ActivityTypes.Where(x => x.Id == typeId).FirstOrDefault();
            if (a.CountingType == "km")
                return true;
            else return false;
        }

        public bool IsTimeType(int typeId)
        {
            var a = ActivityTypes.Where(x => x.Id == typeId).FirstOrDefault();
            if (a.CountingType == "time")
                return true;
            else return false;
        }

        public bool Delete(ActivityType type)
        {
            var curType = GetById(type.Id);

            if (curType == null) return false;

            context.Remove(curType);
            context.SaveChanges();
            return true;
        }

        public bool DeleteById(int Id)
        {
            return Delete(new ActivityType { Id = Id });
        }

        public ActivityType GetById(int Id)
        {
            return context.ActivityTypes.AsQueryable().FirstOrDefault(x => x.Id == Id);
        }

        public ActivityType GetByIdNoTracking(int Id)
        {
            return context.ActivityTypes.AsNoTracking().AsQueryable().FirstOrDefault(x => x.Id == Id);
        }

        public void Update(ActivityType type)
        {
            context.ActivityTypes.Update(type);
            context.SaveChanges();
        }
    }
}
