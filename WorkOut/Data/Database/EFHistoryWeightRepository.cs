﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Data.Database
{
    public class EFHistoryWeightRepository : IHistoryWeightRepository
    {
        private ApplicationDbContext context;

        public EFHistoryWeightRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<HistoryWeight> HistoryWeights => context.HistoryWeights;

        public int Add(HistoryWeight historyWeight)
        {
            context.Add(historyWeight);
            context.SaveChanges();

            return historyWeight.Id;
        }

        public IQueryable<HistoryWeight> GetHistoryWeightsByUser(int userId)
        {
            return (context.HistoryWeights.Where(x => x.UserId == userId));
        }
    }
}
