﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Data.Database
{
    public class EFWorkoutRepository : IWorkoutRepository
    {
        private ApplicationDbContext context;

        public EFWorkoutRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Workout> Workouts => context.Workouts;

        public int AddNumber(Workout workout, Set[] sets)
        {
            context.Add(workout);

            var numbWorkoutr = new NumberWorkout { WorkoutId = workout.Id };
            context.NumberWorkouts.Add(numbWorkoutr);

            foreach (var item in sets)
            {
                item.NumberWorkoutId = numbWorkoutr.Id;
                context.Sets.Add(item);
            }

            context.SaveChanges();

            return workout.Id;
        }

        public int AddDistance(Workout workout, DistanceWorkout distanceWorkout)
        {
            context.Add(workout);

            distanceWorkout.WorkoutId = workout.Id;
            context.Add(distanceWorkout);

            context.SaveChanges();

            return workout.Id;
        }

        public int AddTime(Workout workout)
        {
            context.Add(workout);
            context.SaveChanges();

            return workout.Id;
        }

        public int UpdateNumber(Workout workout, Set[] sets)
        {
            Workout workoutOld = GetById(workout.Id);

            workoutOld.DateTimeStart = workout.DateTimeStart;
            workoutOld.DateTimeFinish = workout.DateTimeFinish;
            workoutOld.Calouries = workout.Calouries;
            workoutOld.Description = workout.Description;
            workoutOld.Pause = workout.Pause;

            context.SaveChanges();

            NumberWorkout numberWorkout = context.NumberWorkouts.FirstOrDefault(x => x.WorkoutId == workout.Id);

            context.Sets.RemoveRange(context.Sets.Where(x => x.NumberWorkoutId == numberWorkout.Id));
            context.SaveChanges();

            foreach (var item in sets)
            {
                item.NumberWorkoutId = numberWorkout.Id;
                context.Sets.Add(item);
                context.SaveChanges();
            }

            return workout.Id;
        }

        public int UpdateDistance(Workout workout, DistanceWorkout distanceWorkout)
        {
            Workout workoutOld = GetById(workout.Id);

            workoutOld.DateTimeStart = workout.DateTimeStart;
            workoutOld.DateTimeFinish = workout.DateTimeFinish;
            workoutOld.Calouries = workout.Calouries;
            workoutOld.Description = workout.Description;
            workoutOld.Pause = workout.Pause;

            context.SaveChanges();

            DistanceWorkout distanceWorkoutOld = context.DistanceWorkouts.FirstOrDefault(x => x.Id == distanceWorkout.Id);

            distanceWorkoutOld.Temp = distanceWorkout.Temp;
            distanceWorkoutOld.Distance = distanceWorkout.Distance;

            context.SaveChanges();

            return workout.Id;
        }

        public int UpdateTime(Workout workout)
        {
            Workout workoutOld = GetById(workout.Id);

            workoutOld.DateTimeStart = workout.DateTimeStart;
            workoutOld.DateTimeFinish = workout.DateTimeFinish;
            workoutOld.Calouries = workout.Calouries;
            workoutOld.Description = workout.Description;
            workoutOld.Pause = workout.Pause;

            context.SaveChanges();

            return workout.Id;
        }

        public bool Delete(Workout workout)
        {
            var curWorkout = GetById(workout.Id);

            if (curWorkout == null) return false;


            if (workout.DistanceWorkout != null)
            {
                context.RemoveRange(context.DistanceWorkouts.Where(x => x.WorkoutId == workout.Id));
            }
            if (workout.NumberWorkout != null)
            {
                NumberWorkout numberWorkout = context.NumberWorkouts.Where(x => x.WorkoutId == workout.Id).Include(x=>x.Sets).FirstOrDefault();

                context.RemoveRange(numberWorkout.Sets);
                context.Remove(numberWorkout);
            }

            context.Remove(curWorkout);
            context.SaveChanges();
            return true;
        }

        public bool DeleteById(int Id)
        {
            return Delete(new Workout { Id = Id });
        }

        public Workout GetById(int id)
        {
            return context.Workouts.AsQueryable().Include(x=>x.ActivityType).Include(x=>x.NumberWorkout).Include(x=>x.DistanceWorkout).FirstOrDefault(x => x.Id == id);
        }
    }
}
