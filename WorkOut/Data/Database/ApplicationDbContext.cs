﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Models;

namespace WorkOut.Data.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<NumberWorkout> NumberWorkouts { get; set; }
        public DbSet<DistanceWorkout> DistanceWorkouts { get; set; }
        public DbSet<HistoryPoint> HistoryPoints { get; set; }
        public DbSet<HistoryWeight> HistoryWeights { get; set; }
    }
}
