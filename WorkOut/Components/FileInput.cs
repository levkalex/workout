﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkOut.Data.Components;

namespace WorkOut.Components
{
   
    public class FileInput : ViewComponent
    {
        public IViewComponentResult Invoke(string accept = "", int maxHeight = 200, string text = "Choose image", bool newLine = true, string image = "")
        {
            if (image == "") image = "/Content/icon/picture.png";
            return View(new FileInputParams() { Accept = accept, MaxHeight = maxHeight, Text = text, NewLine = newLine, Image = image });
        }
    }
}
