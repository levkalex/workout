﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WorkOut.Data.Database;
using WorkOut.Data.Identity;
using WorkOut.Data.Interfaces;
using IdentityDbContext = WorkOut.Data.Identity.IdentityDbContext;

namespace WorkOut
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
                Configuration["Data:WorkOut:ConnectionString"]));

            services.AddDbContext<IdentityDbContext>(options => options.UseSqlServer(
                Configuration["Data:WorkOut:ConnectionString"]));

            services.AddIdentity<User, Role>()
              .AddRoleManager<RoleManager<Role>>()
              .AddUserManager<WUserManager>()
              .AddEntityFrameworkStores<IdentityDbContext>()
              .AddDefaultTokenProviders();

            services.AddTransient<IActivityTypeRepository, EFActivityTypeRepository>();
            services.AddTransient<IWorkoutRepository, EFWorkoutRepository>();
            services.AddTransient<IHistoryPointRepository, EFHistoryPointRepository>();
            services.AddTransient<IHistoryWeightRepository, EFHistoryWeightRepository>();

            services.AddMvc();
        }

       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePagesWithRedirects("/Error/{0}");
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
