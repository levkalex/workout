﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WorkOut.Data.Identity;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;
using WorkOut.Data.ViewModels;

namespace WorkOut.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly WUserManager userManager;
        private readonly IHistoryPointRepository historyPointRepository;
        private readonly IHistoryWeightRepository historyWeightRepository;
        public ProfileController(WUserManager userManager, IHistoryPointRepository historyPointRepository, IHistoryWeightRepository historyWeightRepository)
        {
            this.userManager = userManager;
            this.historyPointRepository = historyPointRepository;
            this.historyWeightRepository = historyWeightRepository;
        }

        public IActionResult Index()
        {
            HistoryWeight lastWeight = historyWeightRepository.HistoryWeights.Where(x=>x.UserId == User.GetUserId()).OrderByDescending(u => u.Id).FirstOrDefault();

            IQueryable<ChartWeight> chartWeight = historyWeightRepository.HistoryWeights.Where(x => x.UserId == User.GetUserId()).Select(x => new { date = x.DateTime.ToShortDateString(), av = x.Weight }).GroupBy(r => r.date).Select(x=> new ChartWeight { Date = x.Key, Weight = x.Average(l => l.av) });
            chartWeight = chartWeight.OrderBy(x=>x.Date).Take(7);

            return View(new ProfileViewModel { User = userManager.GetUserAsync(User).Result, CurrentWeight = lastWeight, ChartWeights = chartWeight });
        }

        public IActionResult ListWeight()
        {
            IQueryable<HistoryWeight> weights = historyWeightRepository.HistoryWeights.Where(x => x.UserId == User.GetUserId()).OrderByDescending(u => u.Id);

            return View(weights);
        }

        [HttpGet]
        public IActionResult Edit()
        {
            return View(userManager.GetUserAsync(User).Result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(User user, IFormFile file)
        {
            User oldUser = userManager.GetUserAsync(User).Result;

            oldUser.FirstName = user.FirstName;
            oldUser.LastName = user.LastName;

            if (file != null)
            {
                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)file.Length);
                }
                oldUser.Avatar = imageData;
            }

            await userManager.UpdateAsync(oldUser);

            return RedirectToAction(nameof(Index));
        }


        public IActionResult HistoryPoint()
        {
            return View(historyPointRepository.HistoryPointsByUser(User.GetUserId()).OrderByDescending(x=>x.DateTime));
        }

        [HttpPost]
        public IActionResult AddWeight(double weight)
        {
            historyWeightRepository.Add(new HistoryWeight { DateTime = DateTime.Now, UserId = User.GetUserId(), Weight = weight });

            return RedirectToAction("Index");
        }



    }
}