﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CarSharing.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{id?}")]
        public IActionResult Error(string id)
        {
            if (id == "404")
                return View("404");
            else
                return View();
        }
    }
}