﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;

namespace WorkOut.Controllers
{
    public class ActivityTypeController : Controller
    {
        IActivityTypeRepository typeRepository;

        public ActivityTypeController(IActivityTypeRepository repository)
        {
            typeRepository = repository;
        }

        public IActionResult Index()
        {
            return View(typeRepository.ActivityTypes);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(ActivityType type, IFormFile file)
        {
            if (file != null)
            {
                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)file.Length);
                }
                type.Icon = imageData;
            }

            typeRepository.Add(type);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            bool succses = typeRepository.DeleteById(id);

            if (!succses) return NotFound();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ActivityType type = typeRepository.GetById(id);

            if (type == null)
            {
                return NotFound();
            }

            

            return View(type);
        }


        [HttpPost]
        public ActionResult Edit(ActivityType type, IFormFile file)
        {
            if (file != null)
            {
                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)file.Length);
                }
                type.Icon = imageData;
            }
            else
            {             
                type.Icon = typeRepository.GetByIdNoTracking(type.Id).Icon;
            }

            typeRepository.Update(type);

            return RedirectToAction("Index");
        }
    }
}