﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WorkOut.Data.Identity;

namespace WorkOut.Controllers
{
    public class UserController : Controller
    {
        private readonly WUserManager userManager;

        public UserController(WUserManager userManager)
        {
            this.userManager = userManager;
        }
       
        [Route("User/Id{Id}")]
        public IActionResult Index(int Id)
        {
            User user = userManager.FindByIdAsync(Id.ToString()).Result;

            if (User.GetUserId() == Id)
                return RedirectToAction("Index", "Profile");
            return View(user);
        }


        [Route("Users")]
        public IActionResult Find()
        {
            IEnumerable<User> users = userManager.Users.Where(x=>x.Id != User.GetUserId());

            return View(users);
        }
    }
}