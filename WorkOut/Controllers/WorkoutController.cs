﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WorkOut.Data.Identity;
using WorkOut.Data.Interfaces;
using WorkOut.Data.Models;
using WorkOut.Data.ViewModels;

namespace WorkOut.Controllers
{
    [Authorize]
    public class WorkoutController : Controller
    {
        IWorkoutRepository workoutRepository;
        IActivityTypeRepository activityTypeRepository;
        private readonly WUserManager userManager;

        public WorkoutController(IWorkoutRepository repository, IActivityTypeRepository activityTypeRepository, WUserManager userManager)
        {
            workoutRepository = repository;
            this.activityTypeRepository = activityTypeRepository;
            this.userManager = userManager;
        }

        public ActionResult Index()
        {
            return View(workoutRepository.Workouts.Where(x => x.UserId == User.GetUserId()).Include(x => x.ActivityType).Include(x => x.NumberWorkout).Include(x => x.NumberWorkout.Sets).Include(x => x.DistanceWorkout).OrderByDescending(x => x.DateTimeStart));
        }

        public ActionResult DetailsDistance(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.Id == id && x.UserId == User.GetUserId()).Include(x => x.DistanceWorkout).Include(x => x.ActivityType).FirstOrDefault();

            if (workout == null)
                return NotFound();

            return View("Distance/DetailsDistance", workout);
        }

        public ActionResult DetailsNumber(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.Id == id && x.UserId == User.GetUserId()).Include(x => x.NumberWorkout).Include(x => x.NumberWorkout.Sets).Include(x => x.ActivityType).FirstOrDefault();

            if (workout == null)
                return NotFound();

            return View("Number/DetailsNumber", workout);
        }


        public ActionResult DetailsTime(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.Id == id && x.UserId == User.GetUserId()).Include(x => x.ActivityType).FirstOrDefault();

            if (workout == null)
                return NotFound();

            return View("Time/DetailsTime", workout);
        }

        public ActionResult Create()
        {
            var activityTypes = activityTypeRepository.ActivityTypes.Select(x => new ActivityTypeViewModel { Id = x.Id, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower(), Icon = x.Icon });
            return View(activityTypes);
        }

        /*COUNTER*/
        [Route("Create/Counter")]
        [HttpGet]
        public ActionResult CreateNumber()
        {
            var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "counter").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
            return View("Number/CreateNumber", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
        }

        [Route("Create/Counter")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateNumber(Workout workout, Set[] sets = null)
        {
            try
            {
                /*POINTS*/
                var activity = activityTypeRepository.GetById(workout.ActivityTypeId.Value);
                int point = 0;

                workout.UserId = User.GetUserId();

                /*SUMMARY COUNT SETS*/
                int sum = 0;
                foreach (var s in sets) sum += s.Value;
                point = (int)Math.Round(activity.Point * sum, MidpointRounding.ToEven);
                workout.Point = point;

                int id = workoutRepository.AddNumber(workout, sets);

                //ADD POINT
                await userManager.AddPointAsync(User, point, "workout", activity.Name);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "counter").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Number/CreateNumber", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }

        /*DISTANCE*/
        [Route("Create/Distance")]
        [HttpGet]
        public ActionResult CreateDistance()
        {
            var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "km").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
            return View("Distance/CreateDistance", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
        }

        [Route("Create/Distance")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateDistance(Workout workout, DistanceWorkout distanceWorkout = null)
        {
            try
            {
                /*POINTS*/
                var activity = activityTypeRepository.GetById(workout.ActivityTypeId.Value);
                int point = 0;

                workout.UserId = User.GetUserId();


                point = (int)Math.Round(activity.Point * distanceWorkout.Distance, MidpointRounding.ToEven);
                workout.Point = point;

                int id = workoutRepository.AddDistance(workout, distanceWorkout);

                //ADD POINT
                await userManager.AddPointAsync(User, point, "workout", activity.Name);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "km").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Distance/CreateDistance", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }


        /*DISTANCE*/
        [Route("Create/Time")]
        [HttpGet]
        public ActionResult CreateTime()
        {
            var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "time").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
            return View("Time/CreateTime", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
        }

        [Route("Create/Time")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateTime(Workout workout)
        {
            try
            {
                /*POINTS*/
                var activity = activityTypeRepository.GetById(workout.ActivityTypeId.Value);
                int point = 0;

                workout.UserId = User.GetUserId();


                point = (int)Math.Round(activity.Point * (workout.DateTimeFinish - workout.DateTimeStart).TotalMinutes, MidpointRounding.ToEven);
                workout.Point = point;

                int id = workoutRepository.AddTime(workout);

                //ADD POINT
                await userManager.AddPointAsync(User, point, "workout", activity.Name);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "time").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Time/CreateTime", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }



        [HttpGet]
        [ActionName("Delete")]
        public IActionResult ConfirmDelete(int id)
        {
            Workout workout = workoutRepository.GetById(id);
            if (workout == null) return NotFound();

            return View(id);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.Id == id && x.UserId == User.GetUserId()).FirstOrDefault();

            if (workout == null)
                return NotFound();

            bool succses = workoutRepository.DeleteById(id);

            if (!succses) return NotFound();

            return RedirectToAction("Index");
        }


        /*EDIT WORKOUT*/
        [Route("Edit/Number")]
        [HttpGet]
        public ActionResult EditNumber(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.UserId == User.GetUserId() && x.Id == id).Include(x => x.ActivityType).Include(x => x.NumberWorkout).Include(x => x.NumberWorkout.Sets).Include(x => x.DistanceWorkout).FirstOrDefault();

            if (workout == null)
                return NotFound();

            var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "counter").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });

            return View("Number/EditNumber", new CreateWorkoutViewModel { ActivityTypes = activityTypes, Workout = workout });
        }

        [Route("Edit/Number")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditNumber(Workout workout, Set[] sets = null)
        {
            try
            {
                if (workoutRepository.Workouts.FirstOrDefault(x => x.Id == workout.Id && x.UserId == User.GetUserId()) == null)
                    return NotFound();

                workoutRepository.UpdateNumber(workout, sets);
                return RedirectToAction(nameof(DetailsNumber), new { Id = workout.Id });
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "counter").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Number/EditNumber", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }

        [Route("Edit/Distance")]
        [HttpGet]
        public ActionResult EditDistance(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.UserId == User.GetUserId() && x.Id == id).Include(x => x.ActivityType).Include(x => x.NumberWorkout).Include(x => x.NumberWorkout.Sets).Include(x => x.DistanceWorkout).FirstOrDefault();

            if (workout == null)
                return NotFound();

            var activityTypes = activityTypeRepository.ActivityTypes.Where(x=>x.CountingType == "km").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });

            return View("Distance/EditDistance", new CreateWorkoutViewModel { ActivityTypes = activityTypes, Workout = workout });
        }

        [Route("Edit/Distance")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDistance(Workout workout, DistanceWorkout distanceWorkout = null)
        {
            try
            {
                if (workoutRepository.Workouts.FirstOrDefault(x => x.Id == workout.Id && x.UserId == User.GetUserId()) == null)
                    return NotFound();

                workoutRepository.UpdateDistance(workout, distanceWorkout);
                return RedirectToAction(nameof(DetailsDistance), new { Id = workout.Id });
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "km").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Distance/EditDistance", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }


        [Route("Edit/Time")]
        [HttpGet]
        public ActionResult EditTime(int id)
        {
            Workout workout = workoutRepository.Workouts.Where(x => x.UserId == User.GetUserId() && x.Id == id).FirstOrDefault();

            if (workout == null)
                return NotFound();

            var activityTypes = activityTypeRepository.ActivityTypes.Where(x => x.CountingType == "time").Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });

            return View("Time/EditTime", new CreateWorkoutViewModel { ActivityTypes = activityTypes, Workout = workout });
        }

        [Route("Edit/Time")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTime(Workout workout, Set[] sets = null, DistanceWorkout distanceWorkout = null)
        {
            try
            {
                workoutRepository.UpdateTime(workout);
                return RedirectToAction(nameof(DetailsTime), new { Id = workout.Id });
            }
            catch
            {
                var activityTypes = activityTypeRepository.ActivityTypes.Select(x => new ActivityTypeViewModel { Id = x.Id, Kcal = x.Kcal, Point = x.Point, CountingType = x.CountingType, NewName = x.Name + " " + x.SubtypeName?.ToLower() });
                return View("Time/EditTime", new CreateWorkoutViewModel { ActivityTypes = activityTypes });
            }
        }
    }
}